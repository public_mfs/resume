import { fileURLToPath, URL } from 'node:url';

import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';

// Gzip压缩
import viteCompression from 'vite-plugin-compression';

//CSS兼容使用autoprefixer添加前缀
import autoprefixer from 'autoprefixer';

// https://vitejs.dev/config/
export default defineConfig({
  server: {
    host: "192.168.31.113",
  },
  plugins: [
    vue(),
    viteCompression({
      filter: /\.(js|css|json|txt|html|ico|svg|png|jpg|jpeg|bin|gltf)(\?.*)?$/i, // 需要压缩的文件
      threshold: 1024, // 文件容量大于这个值进行压缩，单位是bit
      algorithm: 'gzip', // 压缩方式
      ext: 'gz', // 后缀名
      deleteOriginFile: false, // 压缩后是否删除压缩源文件
    }),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  css: {
    postcss: {
      plugins: [
        // 配置 autoprefixer
        autoprefixer({
          overrideBrowserslist: [
            // 'Android 4.1',
            // 'iOS 7.1',//兼容 iOS 7.1 及以上版本的 Safari 浏览器。
            // 'Chrome > 31',//兼容 Chrome 31 及以上版本。
            // 'ff > 31',
            // 'ie >= 8',//兼容 Internet Explorer 8 及以上版本。
            // '> 1%',

            // "> 1%",
            "last 2 versions",
            "not ie <= 8",
          ],
          grid: true,//为 CSS 中的网格布局自动添加前缀
        }),
      ],
    },
  }
})
