//开发环境接口地址
let commonPrefix, protocalDomin;
const hrefObj = new URL(location.href);

if (import.meta.env.DEV) {
    protocalDomin = hrefObj.protocol + '//' + hrefObj.hostname;
    commonPrefix = `${protocalDomin}:1001/`;
} else {
    protocalDomin = hrefObj.protocol + '//' + hrefObj.hostname;
    commonPrefix = protocalDomin + '/backEnd/';
}

export {
    protocalDomin,
    commonPrefix
}

