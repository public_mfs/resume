import './assets/styles/base.less';
import "./assets/styles/commonStyle.css";

import { createApp } from 'vue'
import App from './App.vue'

createApp(App).mount('#app')
