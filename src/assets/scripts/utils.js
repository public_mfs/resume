function getPlatform() {
    const a = {
        versions: (function () {
            var a = navigator.userAgent;
            return {
                iPhone: a.indexOf("iPhone") > -1,
                // || a.indexOf("Mac") > -1,//这里去掉Mac电脑
                iPad: a.indexOf("iPad") > -1,
            };
        })(),
    };
    let b;
    if (a.versions.iPhone || a.versions.iPad) { b = "iphone"; }
    else b = "gphone";
    return b;
}


export {
    getPlatform,
}